package com.indyli.services.spring.rest.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.indyli.services.spring.rest.dto.EmailData;
import com.indyli.services.spring.rest.services.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Service
public class EmailServiceImpl implements EmailService {

    @Value("${spring.mail.username}")
    private String fromEmail;

    //@Resource(name = "javamailSender")
    @Autowired
    private final JavaMailSender javaMailSender;

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public String sendMail(@RequestBody EmailData emailData) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            mimeMessageHelper.setFrom(fromEmail);
            mimeMessageHelper.setTo(InternetAddress.parse(emailData.getTo()));
            mimeMessageHelper.setCc(InternetAddress.parse(String.join(",", emailData.getCc())));
            mimeMessageHelper.setSubject(emailData.getSubject());
            mimeMessageHelper.setText(emailData.getBody(), true);

            if (!Objects.isNull(emailData.getFiles()) && !emailData.getFiles().isEmpty()) {
                for (MultipartFile file : emailData.getFiles()) {
                    mimeMessageHelper.addAttachment(Objects.requireNonNull(file.getOriginalFilename()), file);
                }
            }

            javaMailSender.send(mimeMessage);
            ObjectMapper objectMapper = new ObjectMapper();
            String responseJson = "{ \"status\": \"success\", \"message\": \"E-mail envoyé avec succès!\" }";
            return objectMapper.writeValueAsString(responseJson);
        } catch (MessagingException e) {
            throw new RuntimeException("Erreur lors de l'envoi de l'e-mail", e);
        } catch (Exception e) {
            throw new RuntimeException("Une erreur s'est produite", e);
        }
    }

}

package com.indyli.services.spring.rest.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.indyli.services.spring.rest.dto.BdcBasicDTO;
import com.indyli.services.spring.rest.dto.BdcFullDTO;
import com.indyli.services.spring.rest.dto.BdcMediumDTO;
import com.indyli.services.spring.rest.entities.Bdc;

public interface BdcService<B, B1 extends BdcMediumDTO, B2 extends BdcBasicDTO, B3> extends CRUDService<Bdc, BdcFullDTO, BdcMediumDTO, BdcBasicDTO> {
    String getLastBdc() throws JsonProcessingException;
}

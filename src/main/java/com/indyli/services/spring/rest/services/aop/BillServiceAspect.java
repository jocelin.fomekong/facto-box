package com.indyli.services.spring.rest.services.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.indyli.services.spring.rest.dto.BillFullDTO;
import io.micrometer.common.util.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class BillServiceAspect {

    public static String LINE_SEP = "|";
    Logger logger = LoggerFactory.getLogger(BillServiceAspect.class);

    private StringBuilder errorMessage = null;

    public BillServiceAspect() {
    }

    @Before("execution(* com.indyli.services.spring.rest.services.impl.BillServiceImpl.create(..))")
    public void controlCreateBill(JoinPoint joinPoint) throws JsonProcessingException {
        BillFullDTO billFullDTOToCreate = (BillFullDTO) joinPoint.getArgs()[0];
        this.errorMessage = new StringBuilder();

        if (StringUtils.isBlank(billFullDTOToCreate.getNumeroFacture())) {
            this.errorMessage.append("Veuillez renseigner le numero de la facture lors de la création" + LINE_SEP);
        }

        // Quand on a balayé tous les controles alors on fait un retour global
        String finalErrorMsg = errorMessage.toString();
        if (StringUtils.isNotBlank(finalErrorMsg)) {
            throw new Error(this.errorMessage.toString());
        }
    }


    @Before("execution(* com.indyli.services.spring.rest.services.impl.BillServiceImpl.update(..))")
    public void controlUpdateBill(JoinPoint joinPoint) throws JsonProcessingException {
        BillFullDTO billFullDTOToUpdate = (BillFullDTO) joinPoint.getArgs()[0];
        this.errorMessage = new StringBuilder();

        if (StringUtils.isBlank(billFullDTOToUpdate.getNumeroFacture())) {
            this.errorMessage.append("Veuillez renseigner le numero de la facture lors de la mise à jour" + LINE_SEP);
        }

        // Quand on a balayé tous les controles alors on fait un retour global
        String finalErrorMsg = errorMessage.toString();
        if (StringUtils.isNotBlank(finalErrorMsg)) {
            throw new Error(this.errorMessage.toString());
        }
    }
}

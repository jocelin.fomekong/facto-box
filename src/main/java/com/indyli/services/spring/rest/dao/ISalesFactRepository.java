package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.SalesFact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISalesFactRepository extends JpaRepository<SalesFact, Long> {
}

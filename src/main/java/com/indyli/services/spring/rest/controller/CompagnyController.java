package com.indyli.services.spring.rest.controller;


import com.indyli.services.spring.rest.dto.CompagnyBasicDTO;
import com.indyli.services.spring.rest.dto.CompagnyFullDTO;
import com.indyli.services.spring.rest.dto.CompagnyMediumDTO;
import com.indyli.services.spring.rest.entities.Compagny;
import com.indyli.services.spring.rest.services.CRUDService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "compagnies")
public class CompagnyController {

    private final CRUDService<Compagny, CompagnyFullDTO, CompagnyMediumDTO, CompagnyBasicDTO> compagnyService;

    public CompagnyController(CRUDService<Compagny, CompagnyFullDTO, CompagnyMediumDTO, CompagnyBasicDTO> compagnyService) {
        this.compagnyService = compagnyService;
    }

    @PostMapping
    public ResponseEntity<Long> createCompagny(@RequestBody CompagnyFullDTO compagnyFullDTO) {
        compagnyService.create(compagnyFullDTO);
        return ResponseEntity.ok(compagnyFullDTO.getId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompagnyFullDTO> updateProject(@PathVariable int id, @RequestBody CompagnyFullDTO compagnyFullDTO) throws ChangeSetPersister.NotFoundException {
        //compagnyFullDTO.setId(id);
        CompagnyFullDTO updatedCompagny = compagnyService.update(compagnyFullDTO);
        return ResponseEntity.ok(updatedCompagny);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCompagny(@PathVariable int id) {
        compagnyService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompagnyFullDTO> getCompagnyById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        CompagnyFullDTO compagnyFullDTO = compagnyService.getById(id);
        return ResponseEntity.ok(compagnyFullDTO);
    }

    @GetMapping
    public ResponseEntity<List<CompagnyBasicDTO>> getAllUsers() {
        List<CompagnyBasicDTO> compagny = compagnyService.getAll();
        return ResponseEntity.ok(compagny);
    }
}


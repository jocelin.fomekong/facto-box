package com.indyli.services.spring.rest.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan("com.indyli.services.spring.rest.entities")
@EnableJpaRepositories(basePackages = {"com.indyli.services.spring.rest.dao"})
@ComponentScan(basePackages = {"com.indyli.services.spring.rest.*",
        "com.indyli.services.spring.rest.services.aop"})
public class SpringRestConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}

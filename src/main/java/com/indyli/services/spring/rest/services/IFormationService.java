package com.indyli.services.spring.rest.services;

import com.indyli.services.spring.rest.dto.FormationBasicDTO;
import com.indyli.services.spring.rest.dto.FormationFullDTO;
import com.indyli.services.spring.rest.dto.FormationMediumDTO;
import com.indyli.services.spring.rest.entities.Formation;

public interface IFormationService<B, B1 extends FormationMediumDTO, B2 extends FormationBasicDTO, B3> extends CRUDService<Formation, FormationFullDTO, FormationMediumDTO, FormationBasicDTO> {
}

package com.indyli.services.spring.rest.exceptions;

import org.modelmapper.ValidationException;
import org.springframework.core.annotation.Order;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
@Order(3)
public class RestApiGlobalExeptions {
    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public ResponseEntity<String> handleNotFoundException(ChangeSetPersister.NotFoundException ex) {
        //return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("L'Objet que vous recherché n'existe pas");
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleValidationException(ValidationException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

    // Ajoutez d'autres méthodes d'exception handlers selon vos besoins

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleGenericException(Exception ex) {
        // Log l'exception
        ex.printStackTrace();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Une erreur interne s'est produite. Veuillez réessayer plus tard.");
    }
}


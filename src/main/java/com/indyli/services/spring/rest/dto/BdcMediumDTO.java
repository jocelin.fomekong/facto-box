package com.indyli.services.spring.rest.dto;

import com.indyli.services.spring.rest.entities.Compagny;
import com.indyli.services.spring.rest.entities.Project;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Bdc}
 */
public class BdcMediumDTO extends BdcBasicDTO {
    private Project project;
    private Compagny company;

    public BdcMediumDTO() {
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Compagny getCompany() {
        return company;
    }

    public void setCompany(Compagny company) {
        this.company = company;
    }
}

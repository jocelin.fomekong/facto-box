package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.Bdc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository(value = "ibdcrepository")
public interface IBdcRepository extends JpaRepository<Bdc, Long> {
    @Query("SELECT b.bdcNumber FROM Bdc b ORDER BY b.id DESC LIMIT 1")
    String getLastBdcNumberOrderByIdDesc();
}

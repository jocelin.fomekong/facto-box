package com.indyli.services.spring.rest.dto;

import java.io.Serializable;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Project}
 */
public class ProjectBasicDTO implements Serializable {
    private Long id;
    private String intitule;
    private String description;
    private String periode;
    private String tjm;
    private String nbJours;

    public ProjectBasicDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getTjm() {
        return tjm;
    }

    public void setTjm(String tjm) {
        this.tjm = tjm;
    }

    public String getNbJours() {
        return nbJours;
    }

    public void setNbJours(String nbJours) {
        this.nbJours = nbJours;
    }
}

package com.indyli.services.spring.rest.dto;

import java.io.Serializable;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Bdc}
 */
public class BdcBasicDTO implements Serializable {
    private Long id;
    private String bdcNumber;
    private String generatedDate;
    private String prestationLocation;
    private String signClient;
    private String signPrestataire;
    private String represented;
    private int total;

    public BdcBasicDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBdcNumber() {
        return bdcNumber;
    }

    public void setBdcNumber(String bdcNumber) {
        this.bdcNumber = bdcNumber;
    }

    public String getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(String generatedDate) {
        this.generatedDate = generatedDate;
    }

    public String getPrestationLocation() {
        return prestationLocation;
    }

    public void setPrestationLocation(String prestationLocation) {
        this.prestationLocation = prestationLocation;
    }

    public String getSignClient() {
        return signClient;
    }

    public void setSignClient(String signClient) {
        this.signClient = signClient;
    }

    public String getSignPrestataire() {
        return signPrestataire;
    }

    public void setSignPrestataire(String signPrestataire) {
        this.signPrestataire = signPrestataire;
    }

    public String getRepresented() {
        return represented;
    }

    public void setRepresented(String represented) {
        this.represented = represented;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

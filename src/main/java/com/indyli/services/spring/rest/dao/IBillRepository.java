package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.Bill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IBillRepository extends JpaRepository<Bill, Long> {
}

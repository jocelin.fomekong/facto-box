package com.indyli.services.spring.rest.entities;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table
public class Maintenance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;
    private Integer duree;
    private BigDecimal prix;

    @OneToMany(mappedBy = "maintenance")
    private List<SalesFact> salesFacts;

    public Maintenance() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }

    public List<SalesFact> getSalesFacts() {
        return salesFacts;
    }

    public void setSalesFacts(List<SalesFact> salesFacts) {
        this.salesFacts = salesFacts;
    }
}

package com.indyli.services.spring.rest.services.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.indyli.services.spring.rest.dto.BdcFullDTO;
import io.micrometer.common.util.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class BdcServiceAspect {

    public static String LINE_SEP = "|";
    Logger logger = LoggerFactory.getLogger(BdcServiceAspect.class);

    private StringBuilder errorMessage = null;

    public BdcServiceAspect() {
    }

    @Before("execution(* com.indyli.services.spring.rest.services.impl.BdcServiceImpl.create(..))")
    public void controlCreateBdc(JoinPoint joinPoint) throws JsonProcessingException {
        BdcFullDTO bdcFullDTOToCreate = (BdcFullDTO) joinPoint.getArgs()[0];
        this.errorMessage = new StringBuilder();

        if (StringUtils.isBlank(bdcFullDTOToCreate.getRepresented())) {
            this.errorMessage.append("Veuillez renseigner le representant lors de la création" + LINE_SEP);
        }

        // Quand on a balayé tous les controles alors on fait un retour global
        String finalErrorMsg = errorMessage.toString();
        if (StringUtils.isNotBlank(finalErrorMsg)) {
            throw new Error(this.errorMessage.toString());
        }
    }

    @Before("execution(* com.indyli.services.spring.rest.services.impl.BdcServiceImpl.update(..))")
    public void controlUpdateBdc(JoinPoint joinPoint) throws JsonProcessingException {
        BdcFullDTO bdcFullDTOToCreate = (BdcFullDTO) joinPoint.getArgs()[0];
        this.errorMessage = new StringBuilder();

        if (StringUtils.isBlank(bdcFullDTOToCreate.getRepresented())) {
            this.errorMessage.append("Veuillez renseigner le representant lors de la mise à jour" + LINE_SEP);
        }

        // Quand on a balayé tous les controles alors on fait un retour global
        String finalErrorMsg = errorMessage.toString();
        if (StringUtils.isNotBlank(finalErrorMsg)) {
            throw new Error(this.errorMessage.toString());
        }
    }
}

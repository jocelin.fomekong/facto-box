package com.indyli.services.spring.rest.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.indyli.services.spring.rest.dto.BdcBasicDTO;
import com.indyli.services.spring.rest.dto.BdcFullDTO;
import com.indyli.services.spring.rest.dto.BdcMediumDTO;
import com.indyli.services.spring.rest.entities.Bdc;
import com.indyli.services.spring.rest.services.BdcService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "bdcs")
public class BdcController {

    private final BdcService<Bdc, BdcFullDTO, BdcMediumDTO, BdcBasicDTO> BdcService;

    public BdcController(BdcService<Bdc, BdcFullDTO, BdcMediumDTO, BdcBasicDTO> BdcService) {
        this.BdcService = BdcService;
    }

    @PostMapping
    public ResponseEntity<Long> createBdc(@RequestBody BdcFullDTO bdcFullDTO) {
        BdcService.create(bdcFullDTO);
        return ResponseEntity.ok(bdcFullDTO.getId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<BdcFullDTO> updateBdc(@PathVariable long id, @RequestBody BdcFullDTO bdcFullDTO) throws ChangeSetPersister.NotFoundException {
        //bdcFullDTO.setId(id);
        BdcFullDTO updatedBdc = BdcService.update(bdcFullDTO);
        return ResponseEntity.ok(updatedBdc);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBdc(@PathVariable int id) {
        BdcService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BdcFullDTO> getBdcById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        BdcFullDTO bdcFullDTO = BdcService.getById(id);
        return ResponseEntity.ok(bdcFullDTO);
    }

    @GetMapping
    public ResponseEntity<List<BdcBasicDTO>> getAllBdc() {
        List<BdcBasicDTO> bdcs = BdcService.getAll();
        return ResponseEntity.ok(bdcs);
    }

    @GetMapping(value = "/last-bdc", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getLastBdcNumber() throws JsonProcessingException {
        String last = BdcService.getLastBdc();
        return ResponseEntity.ok(last);
    }
}


package com.indyli.services.spring.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Formation}
 */
public class FormationBasicDTO implements Serializable {
    private Long id;
    private String titre;
    private Integer duree;
    private BigDecimal prix;

    public FormationBasicDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }
}

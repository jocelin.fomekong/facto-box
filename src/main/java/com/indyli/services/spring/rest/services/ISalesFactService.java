package com.indyli.services.spring.rest.services;

import com.indyli.services.spring.rest.dto.SalesFactBasicDTO;
import com.indyli.services.spring.rest.dto.SalesFactFullDTO;
import com.indyli.services.spring.rest.dto.SalesFactMediumDTO;
import com.indyli.services.spring.rest.entities.SalesFact;

public interface ISalesFactService<B, B1 extends SalesFactMediumDTO, B2 extends SalesFactBasicDTO, B3> extends CRUDService<SalesFact, SalesFactFullDTO, SalesFactMediumDTO, SalesFactBasicDTO> {
}

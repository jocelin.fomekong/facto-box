package com.indyli.services.spring.rest.controller;

import com.indyli.services.spring.rest.dto.EmailData;
import com.indyli.services.spring.rest.services.EmailService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "mail")
public class EmailSendController {

    private final EmailService iemailService;

    public EmailSendController(EmailService iemailService) {
        this.iemailService = iemailService;
    }

    @PostMapping(value = "/send", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public String sendMail(@ModelAttribute EmailData emailData) {
        return iemailService.sendMail(emailData);
    }
}

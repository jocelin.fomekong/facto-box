package com.indyli.services.spring.rest.services;

import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;

public interface CRUDService<E, F, M, B> {

    F create(F fullDTO);

    F update(F fullDTO);

    void delete(long id);

    F getById(long id) throws ChangeSetPersister.NotFoundException;

    List<B> getAll();

}

package com.indyli.services.spring.rest.utils;

public enum StatutPaiement {
    EN_ATTENTE,
    PAYEE,
    EN_RETARD
}

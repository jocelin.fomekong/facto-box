package com.indyli.services.spring.rest.services;

import com.indyli.services.spring.rest.dto.EmailData;

public interface EmailService {
    String sendMail(EmailData emailData);
    //String sendMail(MultipartFile file, String to, String[] cc, String subject, String body);
}

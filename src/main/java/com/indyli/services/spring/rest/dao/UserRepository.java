package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

//@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

}

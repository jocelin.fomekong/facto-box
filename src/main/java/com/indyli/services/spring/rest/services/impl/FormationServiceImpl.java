package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.IFormationRepository;
import com.indyli.services.spring.rest.dto.FormationBasicDTO;
import com.indyli.services.spring.rest.dto.FormationFullDTO;
import com.indyli.services.spring.rest.dto.FormationMediumDTO;
import com.indyli.services.spring.rest.entities.Formation;
import com.indyli.services.spring.rest.services.IFormationService;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FormationServiceImpl implements IFormationService<Formation, FormationFullDTO, FormationMediumDTO, FormationBasicDTO> {

    private final ModelMapper modelMapper;

    private final IFormationRepository iFormationRepository;


    public FormationServiceImpl(ModelMapper modelMapper, IFormationRepository iFormationRepository) {
        this.modelMapper = modelMapper;
        this.iFormationRepository = iFormationRepository;
    }

    @Override
    public FormationFullDTO create(FormationFullDTO formationFullDTO) {
        Formation formation = this.modelMapper.map(formationFullDTO, Formation.class);
        formation = this.iFormationRepository.save(formation);
        formationFullDTO.setId(formation.getId());
        return formationFullDTO;
    }

    @Override
    public FormationFullDTO update(FormationFullDTO formationFullDTO) {
        Formation formation = this.modelMapper.map(formationFullDTO, Formation.class);
        Formation existingFormation = this.iFormationRepository.getById(formation.getId());
        this.modelMapper.map(formation, existingFormation);
        formation = this.iFormationRepository.saveAndFlush(formation);
        return this.modelMapper.map(formation, FormationFullDTO.class);
    }

    @Override
    public void delete(long id) {
        this.iFormationRepository.deleteById(id);
    }

    @Override
    public FormationFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        Formation formation = this.iFormationRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(formation, FormationFullDTO.class);
    }

    @Override
    public List<FormationBasicDTO> getAll() {
        List<Formation> formations = this.iFormationRepository.findAll();
        return formations.stream()
                .map(formation -> this.modelMapper.map(formation, FormationBasicDTO.class))
                .collect(Collectors.toList());
    }
}

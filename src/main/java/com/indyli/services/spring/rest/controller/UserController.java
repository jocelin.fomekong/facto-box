package com.indyli.services.spring.rest.controller;

import com.indyli.services.spring.rest.dto.UserBasicDTO;
import com.indyli.services.spring.rest.dto.UserFullDTO;
import com.indyli.services.spring.rest.dto.UserMediumDTO;
import com.indyli.services.spring.rest.entities.User;
import com.indyli.services.spring.rest.services.CRUDService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "users")
public class UserController {

    private final CRUDService<User, UserFullDTO, UserMediumDTO, UserBasicDTO> userService;

    public UserController(CRUDService<User, UserFullDTO, UserMediumDTO, UserBasicDTO> userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@RequestBody UserFullDTO userDTO) {
        userService.create(userDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserFullDTO> updateUser(@PathVariable int id, @RequestBody UserFullDTO userDTO) throws ChangeSetPersister.NotFoundException {
        //userDTO.setId(id);
        UserFullDTO updatedUser = userService.update(userDTO);
        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserFullDTO> getUserById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        UserFullDTO user = userService.getById(id);
        return ResponseEntity.ok(user);
    }

    @GetMapping
    public ResponseEntity<List<UserBasicDTO>> getAllUsers() {
        List<UserBasicDTO> users = userService.getAll();
        return ResponseEntity.ok(users);
    }
}

package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.UserRepository;
import com.indyli.services.spring.rest.dto.UserBasicDTO;
import com.indyli.services.spring.rest.dto.UserFullDTO;
import com.indyli.services.spring.rest.dto.UserMediumDTO;
import com.indyli.services.spring.rest.entities.User;
import com.indyli.services.spring.rest.services.CRUDService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service()
public class CRUDServiceImpl implements CRUDService<User, UserFullDTO, UserMediumDTO, UserBasicDTO> {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    public CRUDServiceImpl(ModelMapper modelMapper, UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
    }

    @Override
    public UserFullDTO create(UserFullDTO fullDTO) {
        User user = this.modelMapper.map(fullDTO, User.class);
        User userToSave = this.userRepository.save(user);
        fullDTO.setId(userToSave.getId());
        return fullDTO;
    }

    @Override
    public UserFullDTO update(UserFullDTO fullDTO) {
        User user = this.modelMapper.map(fullDTO, User.class);
        User existingUser = this.userRepository.getById(user.getId());
        // Crée un PropertyMap pour spécifier les propriétés à ignorer lors de la copie
        PropertyMap<User, User> propertyMap = new PropertyMap<User, User>() {
            protected void configure() {
                skip().setId(null); // Ignore la copie de l'ID de l'utilisateur
                // Ajoute d'autres propriétés à ignorer si nécessaire
            }
        };

        // Applique le PropertyMap lors de la copie des valeurs de user vers existingUser
        this.modelMapper.addMappings(propertyMap);
        this.modelMapper.map(user, existingUser);

        this.userRepository.saveAndFlush(existingUser);
        return this.modelMapper.map(existingUser, UserFullDTO.class);
    }


    @Override
    public void delete(long id) {
        this.userRepository.deleteById(id);
    }

    @Override
    public UserFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        User user = this.userRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(user, UserFullDTO.class);
    }

    @Override
    public List<UserBasicDTO> getAll() {
        List<User> users = this.userRepository.findAll();
        return users.stream()
                .map(user -> this.modelMapper.map(user, UserBasicDTO.class))
                .collect(Collectors.toList());
    }

}

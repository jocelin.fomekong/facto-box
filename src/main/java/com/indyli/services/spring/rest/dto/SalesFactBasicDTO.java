package com.indyli.services.spring.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.SalesFact}
 */
public class SalesFactBasicDTO implements Serializable {
    private Long id;
    private String designation;
    private Integer quantite;
    private BigDecimal prixUnitaire;
    private String sfType;

    public SalesFactBasicDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getSfType() {
        return sfType;
    }

    public void setSfType(String sfType) {
        this.sfType = sfType;
    }
}

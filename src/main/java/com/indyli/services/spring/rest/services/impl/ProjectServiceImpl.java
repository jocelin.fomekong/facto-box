package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.ProjectRepository;
import com.indyli.services.spring.rest.dto.ProjectBasicDTO;
import com.indyli.services.spring.rest.dto.ProjectFullDTO;
import com.indyli.services.spring.rest.dto.ProjectMediumDTO;
import com.indyli.services.spring.rest.entities.Project;
import com.indyli.services.spring.rest.services.ProjectService;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService<Project, ProjectFullDTO, ProjectMediumDTO, ProjectBasicDTO> {

    private final ModelMapper modelMapper;

    private final ProjectRepository iprojectrepository;

    public ProjectServiceImpl(ModelMapper modelMapper, ProjectRepository iprojectrepository) {
        this.modelMapper = modelMapper;
        this.iprojectrepository = iprojectrepository;
    }

    @Override
    public ProjectFullDTO create(ProjectFullDTO fullDTO) {
        Project project = this.modelMapper.map(fullDTO, Project.class);
        Project projectToSave = this.iprojectrepository.save(project);
        fullDTO.setId(projectToSave.getId());
        return fullDTO;
    }

    @Override
    public ProjectFullDTO update(ProjectFullDTO fullDTO) {
        Project project = this.modelMapper.map(fullDTO, Project.class);
        Project existingProject = this.iprojectrepository.getById(project.getId());
        // Crée un PropertyMap pour spécifier les propriétés à ignorer lors de la copie
        PropertyMap<Project, Project> propertyMap = new PropertyMap<Project, Project>() {
            protected void configure() {
                skip().setId(null); // Ignore la copie de l'ID du projet
                // Ajoute d'autres propriétés à ignorer si nécessaire
            }
        };

        // Applique le PropertyMap lors de la copie des valeurs de project vers existingProject
        this.modelMapper.addMappings(propertyMap);
        this.modelMapper.map(project, existingProject);

        this.iprojectrepository.saveAndFlush(existingProject);
        return this.modelMapper.map(existingProject, ProjectFullDTO.class);
    }


    @Override
    public void delete(long id) {
        this.iprojectrepository.deleteById(id);
    }

    @Override
    public ProjectFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        Project project = this.iprojectrepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(project, ProjectFullDTO.class);
    }

    @Override
    public List<ProjectBasicDTO> getAll() {
        List<Project> projects = this.iprojectrepository.findAll();
        return projects.stream()
                .map(project -> this.modelMapper.map(project, ProjectBasicDTO.class))
                .collect(Collectors.toList());
    }

}

package com.indyli.services.spring.rest.dto;

public class SalesFactMediumDTO extends SalesFactBasicDTO {

    //private BillBasicDTO bill;
    private ServiceBasicDTO service;
    private FormationBasicDTO formation;
    private MaintenanceBasicDTO maintenance;
    private ProjectBasicDTO project;

    public SalesFactMediumDTO() {
    }

  /*  public BillBasicDTO getBill() {
        return bill;
    }

    public void setBill(BillBasicDTO bill) {
        this.bill = bill;
    }*/

    public ServiceBasicDTO getService() {
        return service;
    }

    public void setService(ServiceBasicDTO service) {
        this.service = service;
    }

    public FormationBasicDTO getFormation() {
        return formation;
    }

    public void setFormation(FormationBasicDTO formation) {
        this.formation = formation;
    }

    public MaintenanceBasicDTO getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(MaintenanceBasicDTO maintenance) {
        this.maintenance = maintenance;
    }

    public ProjectBasicDTO getProject() {
        return project;
    }

    public void setProject(ProjectBasicDTO project) {
        this.project = project;
    }
}

package com.indyli.services.spring.rest.entities;

import com.indyli.services.spring.rest.utils.StatutPaiement;
import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
public class Bill implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String numeroFacture;
    private Date dateFacturation;
    private BigDecimal montantHT;
    private BigDecimal montantTTC;
    private BigDecimal tauxTVA;

    @Enumerated(EnumType.STRING)
    private StatutPaiement statutPaiement;

    @ManyToOne
    @JoinColumn(name = "compagny_id")
    private Compagny compagny;

    @OneToMany(mappedBy = "bill", cascade = CascadeType.ALL)
    private List<SalesFact> salesFacts;

    public Bill() {
    }

    public void addSalesFact(SalesFact salesFact) {
        salesFacts.add(salesFact);
        salesFact.setBill(this);
    }

    public void removeSalesFact(SalesFact salesFact) {
        salesFacts.remove(salesFact);
        salesFact.setBill(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroFacture() {
        return numeroFacture;
    }

    public void setNumeroFacture(String numeroFacture) {
        this.numeroFacture = numeroFacture;
    }

    public Date getDateFacturation() {
        return dateFacturation;
    }

    public void setDateFacturation(Date dateFacturation) {
        this.dateFacturation = dateFacturation;
    }

    public BigDecimal getMontantHT() {
        return montantHT;
    }

    public void setMontantHT(BigDecimal montantHT) {
        this.montantHT = montantHT;
    }

    public BigDecimal getMontantTTC() {
        return montantTTC;
    }

    public void setMontantTTC(BigDecimal montantTTC) {
        this.montantTTC = montantTTC;
    }

    public BigDecimal getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(BigDecimal tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public StatutPaiement getStatutPaiement() {
        return statutPaiement;
    }

    public void setStatutPaiement(StatutPaiement statutPaiement) {
        this.statutPaiement = statutPaiement;
    }

    public Compagny getCompagny() {
        return compagny;
    }

    public void setCompagny(Compagny compagny) {
        this.compagny = compagny;
    }

    public List<SalesFact> getSalesFacts() {
        return salesFacts;
    }

    public void setSalesFacts(List<SalesFact> salesFacts) {
        this.salesFacts = salesFacts;
    }
}

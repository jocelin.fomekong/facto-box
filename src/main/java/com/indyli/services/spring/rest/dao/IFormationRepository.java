package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.Formation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFormationRepository extends JpaRepository<Formation, Long> {
    Formation findFormationByTitre(String title);
}

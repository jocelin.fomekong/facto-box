package com.indyli.services.spring.rest.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.indyli.services.spring.rest.dao.IBdcRepository;
import com.indyli.services.spring.rest.dto.BdcBasicDTO;
import com.indyli.services.spring.rest.dto.BdcFullDTO;
import com.indyli.services.spring.rest.dto.BdcMediumDTO;
import com.indyli.services.spring.rest.entities.Bdc;
import com.indyli.services.spring.rest.services.BdcService;
import com.indyli.services.spring.rest.services.ProjectService;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service()
public class BdcServiceImpl implements BdcService<Bdc, BdcFullDTO, BdcMediumDTO, BdcBasicDTO> {

    private final ModelMapper modelMapper;

    private final IBdcRepository ibdcrepository;

    private final ProjectService iprojectservice;

    public BdcServiceImpl(ModelMapper modelMapper, IBdcRepository ibdcrepository, ProjectService iprojectservice) {
        this.modelMapper = modelMapper;
        this.ibdcrepository = ibdcrepository;
        this.iprojectservice = iprojectservice;
    }

    @Override
    public BdcFullDTO create(BdcFullDTO bdcFullDTO) {
        Bdc bdc = this.modelMapper.map(bdcFullDTO, Bdc.class);
        bdc.updateTotal();
        bdc = this.ibdcrepository.save(bdc);
        bdcFullDTO.setId(bdc.getId());
        return bdcFullDTO;
    }

    @Override
    public BdcFullDTO update(BdcFullDTO fullDTO) {
        Bdc bdc = this.modelMapper.map(fullDTO, Bdc.class);
        Bdc existingBdc = this.ibdcrepository.getById(bdc.getId());
        this.modelMapper.map(bdc, existingBdc);
        bdc.updateTotal();
        bdc = this.ibdcrepository.saveAndFlush(bdc);
        return this.modelMapper.map(bdc, BdcFullDTO.class);
    }


    @Override
    public void delete(long id) {
        this.ibdcrepository.deleteById(id);
    }

    @Override
    public BdcFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        Bdc bdc = this.ibdcrepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(bdc, BdcFullDTO.class);
    }


    @Override
    public List<BdcBasicDTO> getAll() {
        List<Bdc> bdcs = this.ibdcrepository.findAll();
        return bdcs.stream()
                .map(bdc -> this.modelMapper.map(bdc, BdcBasicDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public String getLastBdc() throws JsonProcessingException {
        String lastBdcNumber = this.ibdcrepository.getLastBdcNumberOrderByIdDesc();


        // Créer un objet ObjectMapper
        ObjectMapper objectMapper = new ObjectMapper();

        // Utiliser la méthode writeValueAsString pour formatter la chaîne en JSON
        return objectMapper.writeValueAsString(lastBdcNumber);
    }
}

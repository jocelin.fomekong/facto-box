package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.IBillRepository;
import com.indyli.services.spring.rest.dao.IFormationRepository;
import com.indyli.services.spring.rest.dto.*;
import com.indyli.services.spring.rest.entities.Bill;
import com.indyli.services.spring.rest.entities.Formation;
import com.indyli.services.spring.rest.entities.SalesFact;
import com.indyli.services.spring.rest.services.IBillService;
import com.indyli.services.spring.rest.services.IFormationService;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BillServiceImpl implements IBillService<Bill, BillFullDTO, BillMediumDTO, BillBasicDTO> {

    private final ModelMapper modelMapper;

    private final IBillRepository iBillRepository;
    private final IFormationRepository iFormationRepository;

    public BillServiceImpl(ModelMapper modelMapper, IBillRepository iBillRepository, IFormationService iFormationService, IFormationRepository iFormationRepository) {
        this.modelMapper = modelMapper;
        this.iBillRepository = iBillRepository;
        this.iFormationRepository = iFormationRepository;
    }

    public BigDecimal calculerMontantTotalFacture(Bill bill) {
        List<SalesFact> lignesFacture = bill.getSalesFacts();
        BigDecimal montantTotal = BigDecimal.ZERO;
        for (SalesFact ligneFacture : lignesFacture) {
            BigDecimal prixUnitaire = ligneFacture.getPrixUnitaire();
            int quantite = ligneFacture.getQuantite();
            BigDecimal montantLigne = prixUnitaire.multiply(BigDecimal.valueOf(quantite));
            montantTotal = montantTotal.add(montantLigne);
        }
        return montantTotal.add(montantTotal.multiply(bill.getTauxTVA()).divide(BigDecimal.valueOf(100)));
    }

    public BigDecimal calculerMontantHTFacture(Bill bill) {
        List<SalesFact> lignesFacture = bill.getSalesFacts();
        BigDecimal montantHT = BigDecimal.ZERO;
        for (SalesFact ligneFacture : lignesFacture) {
            BigDecimal prixUnitaire = ligneFacture.getPrixUnitaire();
            int quantite = ligneFacture.getQuantite();
            BigDecimal montantLigne = prixUnitaire.multiply(BigDecimal.valueOf(quantite));
            montantHT = montantHT.add(montantLigne);
        }
        return montantHT;
    }
   /* @Override
    public BillFullDTO create(BillFullDTO billFullDTO){
        List<SalesFactFullDTO> salesFactList = billFullDTO.getSalesFacts();
        Bill bill = this.modelMapper.map(billFullDTO, Bill.class);
        bill.setSalesFacts(new ArrayList<>());
        // Ajouter les lignes de facture à la facture
        for (SalesFactFullDTO ligneFactureDTO : salesFactList) {
            FormationBasicDTO formationBasicDTO = ligneFactureDTO.getFormation();
            Formation formation = this.modelMapper.map(formationBasicDTO, Formation.class);
            Formation formationToSave = this.iFormationRepository.save(formation);
            SalesFact salesFact = this.modelMapper.map(ligneFactureDTO, SalesFact.class);
            salesFact.setFormation(formationToSave);
            bill.addSalesFact(salesFact);
        }
        BigDecimal montantTTC = this.calculerMontantTotalFacture(bill);
        BigDecimal montantHT = this.calculerMontantHTFacture(bill);
        bill.setMontantHT(montantHT);
        bill.setMontantTTC(montantTTC);
        bill = this.iBillRepository.save(bill);
        billFullDTO.setId(bill.getId());
        return billFullDTO;
    }
*/

    @Transactional
    @Override
    public BillFullDTO create(BillFullDTO billFullDTO) {
        List<SalesFactFullDTO> salesFactList = billFullDTO.getSalesFacts();
        Bill bill = this.modelMapper.map(billFullDTO, Bill.class);
        bill.setSalesFacts(new ArrayList<>());
        List<Formation> formationsToSave = new ArrayList<>();
        for (SalesFactFullDTO ligneFactureDTO : salesFactList) {
            FormationBasicDTO formationBasicDTO = ligneFactureDTO.getFormation();
            Formation formation = this.modelMapper.map(formationBasicDTO, Formation.class);

            // Vérifier si la formation existe déjà dans la base de données
            Formation existingFormation = this.iFormationRepository.findFormationByTitre(formation.getTitre());
            if (existingFormation != null) {
                //formation = existingFormation;
                BeanUtils.copyProperties(formation, existingFormation);
            } else {
                formationsToSave.add(formation);
            }
            SalesFact salesFact = this.modelMapper.map(ligneFactureDTO, SalesFact.class);
            salesFact.setFormation(formation);
            bill.addSalesFact(salesFact);
        }

        // Enregistrer les nouvelles formations en une seule fois
        if (!formationsToSave.isEmpty()) {
            this.iFormationRepository.saveAll(formationsToSave);
        }

        BigDecimal montantTTC = this.calculerMontantTotalFacture(bill);
        BigDecimal montantHT = this.calculerMontantHTFacture(bill);
        bill.setMontantHT(montantHT);
        bill.setMontantTTC(montantTTC);
        bill = this.iBillRepository.save(bill);
        billFullDTO.setId(bill.getId());
        return billFullDTO;
    }


    @Transactional
    @Override
    public BillFullDTO update(BillFullDTO billFullDTO) {
        List<SalesFactFullDTO> salesFactList = billFullDTO.getSalesFacts();
        Bill bill = this.modelMapper.map(billFullDTO, Bill.class);
        Bill existingBill = this.iBillRepository.getById(bill.getId());
        this.modelMapper.map(bill, existingBill);
        bill.setSalesFacts(new ArrayList<>());
        List<Formation> formationsToUpdate = new ArrayList<>();
       // bill.setSalesFacts(new ArrayList<>());
        // Ajouter les lignes de facture à la facture
   /*     for (SalesFactBasicDTO ligneFactureDTO : salesFactList) {
            SalesFact salesFact = this.modelMapper.map(ligneFactureDTO, SalesFact.class);
            bill.addSalesFact(salesFact);
        }*/

        for (SalesFactFullDTO ligneFactureDTO : salesFactList) {
            FormationBasicDTO formationBasicDTO = ligneFactureDTO.getFormation();
            Formation formation = this.modelMapper.map(formationBasicDTO, Formation.class);

            // Vérifier si la formation existe déjà dans la base de données
            Formation existingFormation = this.iFormationRepository.findFormationByTitre(formation.getTitre());
            if (existingFormation != null) {
                //formation = existingFormation;
                BeanUtils.copyProperties(formation, existingFormation);
            } else {
                formationsToUpdate.add(formation);
            }
            SalesFact salesFact = this.modelMapper.map(ligneFactureDTO, SalesFact.class);
            salesFact.setFormation(formation);
            bill.addSalesFact(salesFact);
        }

        // Enregistrer les nouvelles formations en une seule fois
        if (!formationsToUpdate.isEmpty()) {
            this.iFormationRepository.saveAll(formationsToUpdate);
        }

        BigDecimal montantTTC = this.calculerMontantTotalFacture(bill);
        BigDecimal montantHT = this.calculerMontantHTFacture(bill);
        bill.setMontantHT(montantHT);
        bill.setMontantTTC(montantTTC);
        bill = this.iBillRepository.saveAndFlush(bill);
        return this.modelMapper.map(bill, BillFullDTO.class);
    }

    @Override
    public void delete(long id) {
        this.iBillRepository.deleteById(id);
    }

    @Override
    public BillFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        Bill bill = this.iBillRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(bill, BillFullDTO.class);
    }

    @Override
    public List<BillBasicDTO> getAll() {
        List<Bill> bills = this.iBillRepository.findAll();
        return bills.stream()
                .map(bill -> this.modelMapper.map(bill, BillBasicDTO.class))
                .collect(Collectors.toList());
    }
}

package com.indyli.services.spring.rest.services;

import com.indyli.services.spring.rest.dto.BillBasicDTO;
import com.indyli.services.spring.rest.dto.BillFullDTO;
import com.indyli.services.spring.rest.dto.BillMediumDTO;
import com.indyli.services.spring.rest.entities.Bill;

public interface IBillService<B, B1 extends BillMediumDTO, B2 extends BillBasicDTO, B3> extends CRUDService<Bill, BillFullDTO, BillMediumDTO, BillBasicDTO> {
}

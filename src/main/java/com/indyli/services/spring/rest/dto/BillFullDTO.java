package com.indyli.services.spring.rest.dto;

import java.util.List;

public class BillFullDTO extends BillMediumDTO {

    private List<SalesFactFullDTO> salesFacts;

    public BillFullDTO() {
    }

    public List<SalesFactFullDTO> getSalesFacts() {
        return salesFacts;
    }

    public void setSalesFacts(List<SalesFactFullDTO> salesFacts) {
        this.salesFacts = salesFacts;
    }
}

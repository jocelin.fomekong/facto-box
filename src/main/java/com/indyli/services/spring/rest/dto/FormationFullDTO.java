package com.indyli.services.spring.rest.dto;

import java.util.List;

public class FormationFullDTO extends FormationMediumDTO {

    private List<SalesFactBasicDTO> salesFacts;

    public FormationFullDTO() {
    }

    public List<SalesFactBasicDTO> getSalesFacts() {
        return salesFacts;
    }

    public void setSalesFacts(List<SalesFactBasicDTO> salesFacts) {
        this.salesFacts = salesFacts;
    }
}

package com.indyli.services.spring.rest.controller;

import com.indyli.services.spring.rest.dto.FormationBasicDTO;
import com.indyli.services.spring.rest.dto.FormationFullDTO;
import com.indyli.services.spring.rest.dto.FormationMediumDTO;
import com.indyli.services.spring.rest.entities.Formation;
import com.indyli.services.spring.rest.services.IFormationService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "formations")
public class FormationController {

    private final com.indyli.services.spring.rest.services.IFormationService<Formation, FormationFullDTO, FormationMediumDTO, FormationBasicDTO> FormationService;

    public FormationController(IFormationService<Formation, FormationFullDTO, FormationMediumDTO, FormationBasicDTO> FormationService) {
        this.FormationService = FormationService;
    }

    @PostMapping
    public ResponseEntity<Long> createFormation(@RequestBody FormationFullDTO billFullDTO) {
        FormationService.create(billFullDTO);
        return ResponseEntity.ok(billFullDTO.getId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<FormationFullDTO> updateFormation(@PathVariable long id, @RequestBody FormationFullDTO billFullDTO) throws ChangeSetPersister.NotFoundException {
        //billFullDTO.setId(id);
        FormationFullDTO updatedFormation = FormationService.update(billFullDTO);
        return ResponseEntity.ok(updatedFormation);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFormation(@PathVariable int id) {
        FormationService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<FormationFullDTO> getFormationById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        FormationFullDTO billFullDTO = FormationService.getById(id);
        return ResponseEntity.ok(billFullDTO);
    }

    @GetMapping
    public ResponseEntity<List<FormationBasicDTO>> getAllFormation() {
        List<FormationBasicDTO> bills = FormationService.getAll();
        return ResponseEntity.ok(bills);
    }

}

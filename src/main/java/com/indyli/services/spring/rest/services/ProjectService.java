package com.indyli.services.spring.rest.services;

import com.indyli.services.spring.rest.dto.ProjectBasicDTO;
import com.indyli.services.spring.rest.dto.ProjectFullDTO;
import com.indyli.services.spring.rest.dto.ProjectMediumDTO;
import com.indyli.services.spring.rest.entities.Project;

public interface ProjectService<B, B1 extends ProjectMediumDTO, B2 extends ProjectBasicDTO, B3> extends CRUDService<Project, ProjectFullDTO, ProjectMediumDTO, ProjectBasicDTO> {
}

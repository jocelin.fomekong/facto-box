package com.indyli.services.spring.rest.urlbase;

public class RestApiUrlBase {
    public static final String url = "*";

    public static final int maxAge = 3600;
}

package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.Compagny;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompagnyRepository extends JpaRepository<Compagny, Long> {
}

package com.indyli.services.spring.rest.controller;

import com.indyli.services.spring.rest.dto.BillBasicDTO;
import com.indyli.services.spring.rest.dto.BillFullDTO;
import com.indyli.services.spring.rest.dto.BillMediumDTO;
import com.indyli.services.spring.rest.entities.Bill;
import com.indyli.services.spring.rest.services.IBillService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "bills")
public class BillController {

    private final com.indyli.services.spring.rest.services.IBillService<Bill, BillFullDTO, BillMediumDTO, BillBasicDTO> BillService;

    public BillController(IBillService<Bill, BillFullDTO, BillMediumDTO, BillBasicDTO> BillService) {
        this.BillService = BillService;
    }

    @PostMapping
    public ResponseEntity<Long> createBill(@RequestBody BillFullDTO billFullDTO) {
        BillService.create(billFullDTO);
        return ResponseEntity.ok(billFullDTO.getId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<BillFullDTO> updateBill(@PathVariable long id, @RequestBody BillFullDTO billFullDTO) throws ChangeSetPersister.NotFoundException {
        //billFullDTO.setId(id);
        BillFullDTO updatedBill = BillService.update(billFullDTO);
        return ResponseEntity.ok(updatedBill);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBill(@PathVariable int id) {
        BillService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<BillFullDTO> getBillById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        BillFullDTO billFullDTO = BillService.getById(id);
        return ResponseEntity.ok(billFullDTO);
    }

    @GetMapping
    public ResponseEntity<List<BillBasicDTO>> getAllBill() {
        List<BillBasicDTO> bills = BillService.getAll();
        return ResponseEntity.ok(bills);
    }
}

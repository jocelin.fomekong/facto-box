package com.indyli.services.spring.rest.controller;


import com.indyli.services.spring.rest.dto.ProjectBasicDTO;
import com.indyli.services.spring.rest.dto.ProjectFullDTO;
import com.indyli.services.spring.rest.dto.ProjectMediumDTO;
import com.indyli.services.spring.rest.entities.Project;
import com.indyli.services.spring.rest.services.CRUDService;
import com.indyli.services.spring.rest.urlbase.RestApiUrlBase;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = RestApiUrlBase.url, maxAge = RestApiUrlBase.maxAge)
@RestController
@RequestMapping(path = "projects")
public class ProjectController {

    private final CRUDService<Project, ProjectFullDTO, ProjectMediumDTO, ProjectBasicDTO> projectService;

    public ProjectController(CRUDService<Project, ProjectFullDTO, ProjectMediumDTO, ProjectBasicDTO> projectService) {
        this.projectService = projectService;
    }

    @PostMapping
    public ResponseEntity<ProjectFullDTO> createProject(@RequestBody ProjectFullDTO projectDTO) {
        ProjectFullDTO project = projectService.create(projectDTO);
        return ResponseEntity.ok(project);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectFullDTO> updateProject(@PathVariable int id, @RequestBody ProjectFullDTO projectDTO) throws ChangeSetPersister.NotFoundException {
        //projectDTO.setId(id);
        ProjectFullDTO updatedProject = projectService.update(projectDTO);
        return ResponseEntity.ok(updatedProject);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        projectService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectFullDTO> getUserById(@PathVariable int id) throws ChangeSetPersister.NotFoundException {
        ProjectFullDTO project = projectService.getById(id);
        return ResponseEntity.ok(project);
    }

    @GetMapping
    public ResponseEntity<List<ProjectBasicDTO>> getAllUsers() {
        List<ProjectBasicDTO> projects = projectService.getAll();
        return ResponseEntity.ok(projects);
    }
}


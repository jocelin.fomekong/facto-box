package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.CompagnyRepository;
import com.indyli.services.spring.rest.dto.CompagnyBasicDTO;
import com.indyli.services.spring.rest.dto.CompagnyFullDTO;
import com.indyli.services.spring.rest.dto.CompagnyMediumDTO;
import com.indyli.services.spring.rest.entities.Compagny;
import com.indyli.services.spring.rest.services.CRUDService;
import jakarta.annotation.Resource;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service()
public class CompagnyServiceImpl implements CRUDService<Compagny, CompagnyFullDTO, CompagnyMediumDTO, CompagnyBasicDTO> {

    private final ModelMapper modelMapper;
    @Resource(name = "compagnyRepository")
    private CompagnyRepository CompagnyRepository;

    public CompagnyServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CompagnyFullDTO create(CompagnyFullDTO fullDTO) {
        Compagny compagny = this.modelMapper.map(fullDTO, Compagny.class);
        Compagny compagnyToSave = this.CompagnyRepository.save(compagny);
        fullDTO.setId(compagnyToSave.getId());
        return fullDTO;
    }

    @Override
    public CompagnyFullDTO update(CompagnyFullDTO fullDTO) {
        Compagny compagny = this.modelMapper.map(fullDTO, Compagny.class);
        Compagny existingCompagny = this.CompagnyRepository.getById(compagny.getId());
        // Crée un PropertyMap pour spécifier les propriétés à ignorer lors de la copie
        PropertyMap<Compagny, Compagny> propertyMap = new PropertyMap<Compagny, Compagny>() {
            protected void configure() {
                skip().setId(null); // Ignore la copie de l'ID de la compagnie
                // Ajoute d'autres propriétés à ignorer si nécessaire
            }
        };

        // Applique le PropertyMap lors de la copie des valeurs de compagny vers existingCompagny
        this.modelMapper.addMappings(propertyMap);
        this.modelMapper.map(compagny, existingCompagny);

        this.CompagnyRepository.saveAndFlush(existingCompagny);
        return this.modelMapper.map(existingCompagny, CompagnyFullDTO.class);
    }

    @Override
    public void delete(long id) {
        this.CompagnyRepository.deleteById(id);
    }

    @Override
    public CompagnyFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        Compagny compagny = this.CompagnyRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(compagny, CompagnyFullDTO.class);
    }

    @Override
    public List<CompagnyBasicDTO> getAll() {
        List<Compagny> companies = this.CompagnyRepository.findAll();
        return companies.stream()
                .map(compagny -> this.modelMapper.map(compagny, CompagnyBasicDTO.class))
                .collect(Collectors.toList());
    }
}

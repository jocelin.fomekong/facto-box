package com.indyli.services.spring.rest.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class EmailData {
    private List<MultipartFile> files;
    private String to;
    private List<String> cc;
    private String subject;
    private String body;

    // Constructeur par défaut
    public EmailData() {
    }

    // Getters et Setters
    public List<MultipartFile> getFiles() {
        return files;
    }

    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

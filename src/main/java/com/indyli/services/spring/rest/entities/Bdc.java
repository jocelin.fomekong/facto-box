package com.indyli.services.spring.rest.entities;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "bdc_ordered")
public class Bdc implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String bdcNumber;
    private Date generatedDate;
    private String prestationLocation;
    private String signClient;
    private String signPrestataire;
    private String represented;
    private int total;

    // Ratachement avec l'entité Project
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id")
    private Project project;
    // Ratachement avec l'entité Company
    @ManyToOne()
    @JoinColumn(name = "company_id")
    private Compagny company;

    public Bdc() {
    }

    public void updateTotal() {
        if (project != null) {
            int nombreJours = project.getNbJours();
            int prixJournalier = project.getTjm();
            total = nombreJours * prixJournalier;
        }
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Compagny getCompany() {
        return company;
    }

    public void setCompany(Compagny company) {
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBdcNumber() {
        return bdcNumber;
    }

    public void setBdcNumber(String bdcNumber) {
        this.bdcNumber = bdcNumber;
    }

    public String getPrestationLocation() {
        return prestationLocation;
    }

    public void setPrestationLocation(String prestationLocation) {
        this.prestationLocation = prestationLocation;
    }

    public String getSignClient() {
        return signClient;
    }

    public void setSignClient(String signClient) {
        this.signClient = signClient;
    }

    public String getSignPrestataire() {
        return signPrestataire;
    }

    public void setSignPrestataire(String signPrestataire) {
        this.signPrestataire = signPrestataire;
    }

    public String getRepresented() {
        return represented;
    }

    public void setRepresented(String represented) {
        this.represented = represented;
    }

    public Date getGeneratedDate() {
        return generatedDate;
    }

    public void setGeneratedDate(Date generatedDate) {
        this.generatedDate = generatedDate;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

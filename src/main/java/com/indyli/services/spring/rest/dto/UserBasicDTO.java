package com.indyli.services.spring.rest.dto;

import java.io.Serializable;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.User}
 */
public class UserBasicDTO implements Serializable {
    private Long id;
    private String username;
    private String login;
    private String firstName;
    private String lastName;
    private String email;

    public UserBasicDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

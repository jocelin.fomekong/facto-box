package com.indyli.services.spring.rest.services.impl;

import com.indyli.services.spring.rest.dao.ISalesFactRepository;
import com.indyli.services.spring.rest.dto.SalesFactBasicDTO;
import com.indyli.services.spring.rest.dto.SalesFactFullDTO;
import com.indyli.services.spring.rest.dto.SalesFactMediumDTO;
import com.indyli.services.spring.rest.entities.SalesFact;
import com.indyli.services.spring.rest.services.ISalesFactService;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesFactServiceImpl implements ISalesFactService<SalesFact, SalesFactFullDTO, SalesFactMediumDTO, SalesFactBasicDTO> {
    private final ModelMapper modelMapper;

    private final ISalesFactRepository iSalesFactRepository;

    public SalesFactServiceImpl(ModelMapper modelMapper, ISalesFactRepository iSalesFactRepository) {
        this.modelMapper = modelMapper;
        this.iSalesFactRepository = iSalesFactRepository;
    }

    @Override
    public SalesFactFullDTO create(SalesFactFullDTO salesFactFullDTO) {
        SalesFact salesFact = this.modelMapper.map(salesFactFullDTO, SalesFact.class);
        salesFact = this.iSalesFactRepository.save(salesFact);
        salesFactFullDTO.setId(salesFact.getId());
        return salesFactFullDTO;
    }

    @Override
    public SalesFactFullDTO update(SalesFactFullDTO salesFactFullDTO) {
        SalesFact salesFact = this.modelMapper.map(salesFactFullDTO, SalesFact.class);
        SalesFact existingSalesFact = this.iSalesFactRepository.getById(salesFact.getId());
        this.modelMapper.map(salesFact, existingSalesFact);
        salesFact = this.iSalesFactRepository.saveAndFlush(salesFact);
        return this.modelMapper.map(salesFact, SalesFactFullDTO.class);
    }

    @Override
    public void delete(long id) {
        this.iSalesFactRepository.deleteById(id);
    }

    @Override
    public SalesFactFullDTO getById(long id) throws ChangeSetPersister.NotFoundException {
        SalesFact salesFact = this.iSalesFactRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);
        return this.modelMapper.map(salesFact, SalesFactFullDTO.class);
    }

    @Override
    public List<SalesFactBasicDTO> getAll() {
        List<SalesFact> salesFacts = this.iSalesFactRepository.findAll();
        return salesFacts.stream()
                .map(salesFact -> this.modelMapper.map(salesFact, SalesFactBasicDTO.class))
                .collect(Collectors.toList());
    }
}

package com.indyli.services.spring.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Service}
 */
public class ServiceBasicDTO implements Serializable {
    private Long id;
    private String nom;
    private String description;
    private BigDecimal prix;

    public ServiceBasicDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }
}
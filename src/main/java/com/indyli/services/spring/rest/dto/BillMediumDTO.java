package com.indyli.services.spring.rest.dto;

public class BillMediumDTO extends BillBasicDTO {
    private CompagnyBasicDTO compagny;

    public BillMediumDTO() {
    }

    public CompagnyBasicDTO getCompagny() {
        return compagny;
    }

    public void setCompagny(CompagnyBasicDTO compagny) {
        this.compagny = compagny;
    }
}

package com.indyli.services.spring.rest.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Maintenance}
 */
public class MaintenanceBasicDTO implements Serializable {
    private Long id;
    private String description;
    private Integer duree;
    private BigDecimal prix;

    public MaintenanceBasicDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal prix) {
        this.prix = prix;
    }
}
package com.indyli.services.spring.rest.dao;

import com.indyli.services.spring.rest.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

//@Repository("projectRepository")
public interface ProjectRepository extends JpaRepository<Project, Long> {
}

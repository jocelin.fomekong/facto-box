package com.indyli.services.spring.rest.exceptions;

public class BdcException extends Exception {
    public BdcException(String msg) {
        super(msg);
    }

    public BdcException(String msg, Throwable t) {
        super(msg, t);
    }
}

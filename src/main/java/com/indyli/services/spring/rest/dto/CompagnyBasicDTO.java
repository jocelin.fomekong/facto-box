package com.indyli.services.spring.rest.dto;

import java.io.Serializable;

/**
 * DTO for {@link com.indyli.services.spring.rest.entities.Compagny}
 */
public class CompagnyBasicDTO implements Serializable {
    private Long id;
    private String societe;
    private String rccm;
    private String forme;
    private String contact;
    private String telephone;

    public CompagnyBasicDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSociete() {
        return societe;
    }

    public void setSociete(String societe) {
        this.societe = societe;
    }

    public String getRccm() {
        return rccm;
    }

    public void setRccm(String rccm) {
        this.rccm = rccm;
    }

    public String getForme() {
        return forme;
    }

    public void setForme(String forme) {
        this.forme = forme;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
